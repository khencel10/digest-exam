<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Digest Exam</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Beth+Ellen&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cardo&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        body { 
            /* font: Montserrat; */
            font-family: 'Beth Ellen', cursive;
            /* font-family: 'Cardo', serif; */
            font-style: italic;
            /* background-color: black; */
            
         }
         .text-secondary{
             color:black !important;
             text-align: justify;
         }
         
        .container-content{
            background-image: url('http://www.sclance.com/backgrounds/old-paper-background-hd/old-paper-background-hd_1637222.png');
            background-repeat: no-repeat;
            background-size: cover;
            background-position: center center;
            box-shadow: 0px 0px 5px black;

        }

        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input { 
        opacity: 0;
        width: 0;
        height: 0;
        }

        .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
        }
        .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
        }

        input:checked + .slider {
        background-color: #2196F3;
        }

        input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
        }

        .slider.round {
        border-radius: 34px;
        }

        .slider.round:before {
        border-radius: 50%;
        }

       
        
    </style>
</head>
<body>
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-5">
                <label class="switch">
                    <input type="checkbox" id="switch">
                    <span class="slider round"></span>
                </label>
                <span class="font-weight-bold" id="switchLabel">
                    Switch to Dark Mode
                </span>
                
        </div>
    </div>
  <div class="jumbotron container-content">
    <div class="row">
        <div class="col-md-10">
            <h5><b>Digest of:</b>
                <h6 class="text-secondary">{{$digest->title}}</h6>
            </h5>
        </div>

        <div class="col-md-2">
            <h6><b>Author:<span class="text-secondary"> {{$digest->author}}</span></b></h6>
            <h6><b>Subject:<span class="text-secondary"> {{$digest->category}}</span></b></h6>
            <h6><b>Rating:<span class="text-secondary"> {{$digest->rating}}</span></b></h6>
        </div>

        <div class="col-md-9">
            <h5><b>Summary:</b>
                
                <h6 class="text-secondary">{{$digest->summary}}</h6>
            </h5>
            <br>
            <h5 class="text-secondary"><b>Doctrine:</b>
                <h6 class="text-secondary">{{$digest->summary}}</h6>
            </h5>
            <br>
            <h5 class="text-secondary"><b>Facts:</b>
                <h6 class="text-secondary">{{$digest->facts}}</h6>
            </h5>
            <br>
            <h5 class="text-secondary"><b>Issues_ratio:</b>
                <h6 class="text-secondary">{{$digest->issues_ratio}}</h6>
            </h5>
            <br>
            <h5 class="text-secondary"><b>Dispositive:</b>
                <h6 class="text-secondary">{{$digest->dispositive}}</h6>
            </h5>
            <br>
            <h5 class="text-secondary"><b>Other Notes:</b>
                <h6 class="text-secondary">{{$digest->note}}</h6>
            </h5>

        </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function(){
        $('#switch').click(function(){
            
            if($(this). prop("checked") == true){
                //dark mode
                $('.container-content').css('background-blend-mode','overlay')
                $('.container-content').css('background-color','black')
                $('.container-content').css('color','white')
                $('.container-content').css('box-shadow','0px 0px 5px white')
                $('body').css('font-family','Cardo')
                $('body').css('background-color','black')
                $(".text-secondary").removeClass('text-secondary');
                $('#switchLabel').html('Switch to Light Mode');
                $('#switchLabel').addClass('text-white');
                $('#switchLabel').removeClass('text-dark');
               
            }else{
                //light mode 
                $('.container-content').css('background-blend-mode','normal')
                $('.container-content').css('background-color','none;')
                $('.container-content').css('color','black')
                $('body').css('font-family','Beth Ellen','cursive')
                $('.container-content').css('box-shadow','0px 0px 5px black')
                $('body').css('background-color','white')
                $('#switchLabel').html('Switch to Dark Mode');
                $('#switchLabel').addClass('text-dark');
                $('#switchLabel').removeClass('text-white');
                
            }
        });

        
    })
</script>
</body>
</html>
